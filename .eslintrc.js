module.exports = {
    extends: 'airbnb-base',
    rules: {
        'comma-dangle': 0,
        'linebreak-style': 0,
        indent: 0,
        'no-multiple-empty-lines': 0,
        'import/no-extraneous-dependencies': 0,
        'no-trailing-spaces': 0,
        'padded-blocks': 0,
    },
};
