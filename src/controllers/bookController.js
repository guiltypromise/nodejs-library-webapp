
const { MongoClient, ObjectId } = require('mongodb');
const debug = require('debug')('app:bookController'); // debugging logs

function bookController(nav) {
    function sessionGuard(req, res, next) {
        if (req.user) {
            next(); // any 'use' must pass a next() or risk hanging up the app
        } else {
            res.redirect('/');
        }
    }


    function getIndex(req, res) {
        // mongo DB url connection
        const url = 'mongodb://localhost:27017';
        const dbName = 'libraryApp';

        (async function mongo() {
            let client;
            try {
                client = await MongoClient.connect(url);
                debug(`Connected to MongoDB server for port: ${27017}`);

                const db = client.db(dbName);

                // return the results of insertion
                const collection = await db.collection('books');

                const books = await collection.find().toArray(); // get all books back

                res.render('bookListView', {
                    nav,
                    title: 'Books',
                    books,
                    sessionUser: req.user
                });
            } catch (error) {
                debug(`${error.stack}`);
                res.status(400).json(error);
            }
        }());
    }

    function getById(req, res) {
        const { id } = req.params;
        const url = 'mongodb://localhost:27017';
        const dbName = 'libraryApp';
        (async function mongo() {
            let client;
            try {
                client = await MongoClient.connect(url);
                debug(`Connected to MongoDB server for port: ${27017}`);

                const db = client.db(dbName);

                // return the results of insertion
                const collection = await db.collection('books');

                // search for the one particular book
                const book = await collection.findOne({ _id: new ObjectId(id) });
                debug(book);

                res.render('bookView', {
                    nav,
                    title: 'Book',
                    book,
                    sessionUser: req.user
                });
            } catch (error) {
                debug(`${error.stack}`);
                res.status(400).json(error);
            }
        }());
    }

    // revealing module pattern, like angularJS
    return {
        sessionGuard,
        getIndex,
        getById
    };
}

module.exports = bookController;
