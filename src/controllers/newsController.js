
const debug = require('debug')('app:newsController'); // debugging logs


function newsController(newsApiService, nav) {

    function getLatestNews(req, res) {
        (async function newsApi() {
            try {
              debug(`News controller has service: ${JSON.stringify(newsApiService)}`);

               const response = await newsApiService.getLatestArticles();
               debug(response.articles);

                res.render('newsView', {
                    nav,
                    title: 'News',
                    articles: response.articles,
                    sessionUser: req.user
                });
            } catch (error) {
                debug(`${error}`);
                res.status(400).json(error.stack);
            }
        }());
    }


    return {
        getLatestNews
    };
}

module.exports = newsController;
