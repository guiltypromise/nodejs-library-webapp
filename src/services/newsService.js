const debug = require('debug')('app:newsService'); // debugging logs
const axios = require('axios');

function newsService() {
    debug('News Service running');

    function getLatestArticles() {
        return new Promise((resolve, reject) => {

            axios.get('https://newsapi.org/v2/top-headlines?country=us&apiKey=eae65f0c1da14a88a96109bed4d8c978')
                .then((response) => {
                    resolve(response.data);
                })
                .catch((error) => {
                    debug(error);
                    reject(error);
                });
        });
    }

    return {
        getLatestArticles 
    };
}

// execute the service here
module.exports = newsService();
