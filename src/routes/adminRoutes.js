
const express = require('express'); // let's us use express server
const { MongoClient } = require('mongodb');
const debug = require('debug')('app:adminRoutes'); // debugging logs

const adminRouter = express.Router(); // encapsulate Admin routes



function router() {
    const books = [
        {
            title: 'Harry Potter and the Sorcerers Stone',
            genre: 'Fantasy',
            author: 'J.K. Rowling',
            read: false,
        },
        {
            title: 'Grapes of Wrath',
            genre: 'Historical fiction',
            author: 'John Steinbeck',
            read: false,
        },
        {
            title: 'The Giver',
            genre: 'Fiction',
            author: 'Lois Lowry',
            read: false,
        }
    ];
    adminRouter.route('/')
        .get((req, res) => {
            // mongo DB url connection
            const url = 'mongodb://localhost:27017';
            const dbName = 'libraryApp';

            (async function mongo() {
                let client;
                try {
                    client = await MongoClient.connect(url);
                    debug(`Connected to MongoDB server for port: ${27017}`);

                    const db = client.db(dbName);

                    // return the results of insertion
                    const response = await db.collection('books').insertMany(books);

                    res.json(response); // output as JSON
                } catch (error) {
                    debug(`${error.stack}`);
                    res.status(400).json(error);
                }
                client.close();
            }());
        });
        return adminRouter;
}

module.exports = router;
