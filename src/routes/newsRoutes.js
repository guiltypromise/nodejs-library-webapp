
const express = require('express'); // let's us use express server

const newsController = require('../controllers/newsController');

const newsRouter = express.Router(); // encapsulate news routes

const newsApiService = require('../services/newsService');

function router(nav) {
    const { getLatestNews } = newsController(newsApiService, nav);

    newsRouter.route('/').get(getLatestNews);

    //newsRouter.route('/)

    return newsRouter;
}

module.exports = router;

