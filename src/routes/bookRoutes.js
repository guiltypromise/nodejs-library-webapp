
const express = require('express'); // let's us use express server

const bookController = require('../controllers/bookController');

const bookRouter = express.Router(); // encapsulate Book routes

function router(nav) {
    const { getIndex, getById, sessionGuard } = bookController(nav);

    // catch all middleware for all these routes
    bookRouter.use(sessionGuard);

    bookRouter.route('/').get(getIndex);

    bookRouter.route('/:id([0-9a-fA-F]{24})').get(getById);

    return bookRouter;
}

module.exports = router;

