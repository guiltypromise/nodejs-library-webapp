const express = require('express');
const { MongoClient, ObjectId } = require('mongodb');
const debug = require('debug')('app:authRoutes');
const passport = require('passport');

const authRouter = express.Router();

function router(nav) {
    authRouter.route('/signUp')
        .post((req, res) => {
            // take in user/pw form submit
            debug(req.body);

            const { username, password } = req.body;
            const url = 'mongodb://localhost:27017';
            const dbName = 'libraryApp';

            (async function addUser() {
                let client;
                try {
                    client = await MongoClient.connect(url);
                    debug(`Connected to MongoDB server for port: ${27017}`);

                    const db = client.db(dbName);

                    const col = db.collection('users');
                    const user = { username, password };

                    // insertOne new user account
                    const results = await col.insertOne(user);

                    const userResult = await col.findOne({ _id: new ObjectId(results.insertedId) });

                    debug(`User inserted: ${JSON.stringify(userResult)}`);

                    // create user
                    req.login(userResult, () => {
                        // after user logged in
                        res.redirect('/auth/profile'); // send browser redirect command
                    });


                } catch (error) {
                    debug(`${error.stack}`);
                    res.status(400).json(error);
                }
            }());
        });

    authRouter.route('/signIn')
        .get((req, res) => {
            res.render('signInView', {
                nav,
                title: 'Sign In',
                sessionUser: req.user
            });
        })
        .post(passport.authenticate('local', {
            successRedirect: '/auth/profile',
            failureRedirect: '/',

        }));
    authRouter.route('/profile')
        .all((req, res, next) => { // execute route middleware "guard"
            if (req.user) {
                next();
            } else {
                res.redirect('/');
            }
        }) 
        .get((req, res) => {
            res.json(req.user);
        });

    authRouter.route('/logout')
        .all((req, res, next) => {
            if (req.user) {
                next();
            } else {
                res.redirect('/');
            }
        })
        .get((req, res) => {
            (async function logoutUser() {
                try {
                    debug(`User to logout: ${JSON.stringify(req.user)}`);

                    // logout session user
                    req.logout(req.user, () => {
                        // after user logged in
                        res.redirect('/'); // send browser redirect command
                    });
                } catch (error) {
                    debug(`${error.stack}`);
                    res.status(400).json(error);
                }
            }());
        });

    return authRouter;
}

module.exports = router;
