
const chalk = require('chalk'); // use colors, only v4 of chalk
const debug = require('debug')('app:mysql'); // debugging logs

// eslint-disable-next-line import/no-extraneous-dependencies
const mysql = require('mysql2/promise');


const dbConfig = {
    host: 'localhost',
    user: 'root',
    port: 3306,
    database: 'library',
    password: ''
};

const dbPool = mysql.createPool(dbConfig);

(function main() {
    debug(`MySQL connected on port ${chalk.green(3306)}`);
}());



module.exports = dbPool;
