const passport = require('passport');

// TODO other ways to passport (Twitter, facebook, oauth..)
require('./strategies/local.strategy')();

module.exports = function passportConfig(app) {
    app.use(passport.initialize()); // creates stuff, one of them is 'login' on req
    app.use(passport.session());


    // store user in session
    passport.serializeUser((user, doneCallback) => {
        // often we only want to store a piece of the user
        // doneCallback(null, user.id)
        doneCallback(null, user);
    });

    // retrieve user from session
    passport.deserializeUser((user, doneCallback) => {
        // find the user by ID, maybe later

        
        doneCallback(null, user);

    });


};
