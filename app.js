// app.js or server.js

const express = require('express'); // let's us use express server
const chalk = require('chalk'); // use colors, only v4 of chalk
const debug = require('debug')('app'); // debugging logs
const morgan = require('morgan'); // log web traffic
const path = require('path');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const session = require('express-session');

// instance of express
const app = express();
const port = process.env.PORT || 3000;


// middleware web traffic debugging
app.use(morgan('tiny')); // combined is everything, tiny is less info
app.use(bodyParser.json()); // body parser boilerplate
app.use(bodyParser.urlencoded({ extended: false }));

app.use(cookieParser());
app.use(session({ secret: 'library' })); // session takes a 'secret' to build a cookie

require('./src/config/passport')(app); // modularized passport middleware

app.use(express.static(path.join(__dirname, '/public'))); // let express know we will use static dirs for static files

app.use('/css', express.static(path.join(__dirname, 'node_modules/bootstrap/dist/css')));
app.use('/js', express.static(path.join(__dirname, 'node_modules/bootstrap/dist/js')));
app.use('/js', express.static(path.join(__dirname, 'node_modules/jquery/dist')));

app.set('views', './src/views');
app.set('view engine', 'ejs');



const nav = [
    { link: '/books', title: 'Books' },
    { link: '/authors', title: 'Authors' },
    { link: '/news', title: 'News' }
];

// wire up all the routers
const bookRouter = require('./src/routes/bookRoutes')(nav);
const newsRouter = require('./src/routes/newsRoutes')(nav);
const adminRouter = require('./src/routes/adminRoutes')();
const authRouter = require('./src/routes/authRoutes')(nav);

app.use('/books', bookRouter);
app.use('/news', newsRouter);
app.use('/admin', adminRouter);
app.use('/auth', authRouter);

// initial home page
app.get('/', (req, res) => {
    res.render('index', {
        nav,
        title: 'Library',
        sessionUser: req.user
    });
}); // browser sends GET request to '/'

// set up listener on a port
app.listen(port, () => {
    debug(`Listening on port ${chalk.green(port)}`);
});
